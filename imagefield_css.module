<?php
/**
 * @file
 * An Imagefield formatter to display image through CSS path.
 */

/**
 * Implements hook_field_formatter_info().
 *
 * @see imagefield_css_field_formatter_view()
 */
function imagefield_css_field_formatter_info() {
  return array(
    // This formatter changes the background image of the content region.
    'imagefield_css_declaration' => array(
      'label' => t('CSS Declaration'),
      'field types' => array('image'),
      'settings' => array(
        'selector' => 'body',
        'image_style' => '',
        'color' => '',
        'repeat' => '',
        'attachment' => '',
        'horizontal_position' => '',
        'vertical_position' => '',
        'important' => TRUE,
      ),
    ),
  );
}


/**
 * Implements hook_theme().
 */
function imagefield_css_theme() {
  return array(
    'imagefield_css_formatter' => array(
      'variables' => array(
        'item' => NULL,
        'path' => NULL,
        'image_style' => NULL,
        'settings' => array(),
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function imagefield_css_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  $element['selector'] = array(
    '#title' => t('Selector'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $settings['selector'],
    '#required' => TRUE,
    '#description' => t('A valid CSS selector such as <code>.links > li > a, #logo</code>.'),
  );
  $element['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tokens'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $element['tokens']['help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('node'),
  );
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style'],
    '#required' => FALSE,
    '#empty_option' => t('None (original image)'),
    '#options' => image_style_options(FALSE),
  );
  $element['color'] = array(
    '#title' => t('Color'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $settings['color'],
    '#required' => FALSE,
    '#description' => t('A valid CSS color value.'),
  );
  $element['repeat'] = array(
    '#title' => t('Repeat'),
    '#type' => 'select',
    '#default_value' => $settings['repeat'],
    '#required' => FALSE,
    '#empty_option' => t('-No value-'),
    '#options' => array(
      'repeat' => t('Repeat horizontally and vertically'),
      'repeat-x' => t('Repeat horizontally'),
      'repeat-y' => t('Repeat vertically'),
      'no-repeat' => t('Do not repeat'),
      'inherit' => t('Inherit from parent element'),
    ),
  );
  $element['attachment'] = array(
    '#title' => t('Attachment'),
    '#type' => 'select',
    '#default_value' => $settings['attachment'],
    '#required' => FALSE,
    '#empty_option' => t('-No value-'),
    '#options' => array(
      'scroll' => t('Scroll with the page'),
      'fixed' => t('Do not scroll'),
      'inherit' => t('Inherit from parent element'),
    ),
  );
  $element['horizontal_position'] = array(
    '#title' => t('Horizontal position'),
    '#type' => 'textfield',
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => $settings['horizontal_position'],
    '#required' => FALSE,
    '#description' => t('A valid CSS horizontal position value.'),
  );
  $element['vertical_position'] = array(
    '#title' => t('Vertical position'),
    '#type' => 'textfield',
    '#size' => 20,
    '#maxlength' => 20,
    '#default_value' => $settings['vertical_position'],
    '#required' => FALSE,
    '#description' => t('A valid CSS vertical position value.'),
  );
  $element['important'] = array(
    '#title' => t('Important'),
    '#type' => 'checkbox',
    '#default_value' => $settings['important'],
    '#description' => t('Whenever this declaration is more important than others.'),
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function imagefield_css_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $summary[] = t('CSS selector') . ': ' . filter_xss_admin($settings['selector']);

  $image_styles = image_style_options(FALSE);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (isset($image_styles[$settings['image_style']])) {
    $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['image_style']]));
  }
  else {
    $summary[] = t('Original image');
  }

  if ($settings['color']) {
    $summary[] = t('Color') . ': ' . filter_xss_admin($settings['color']);
  }
  if ($settings['repeat']) {
    $summary[] = t('Repeat') . ': ' . filter_xss_admin($settings['repeat']);
  }
  if ($settings['attachment']) {
    $summary[] = t('Attachment') . ': ' . filter_xss_admin($settings['attachment']);
  }
  // Display this setting only if horizontal or vertical position is set.
  if ($settings['horizontal_position'] || $settings['vertical_position']) {
    $summary[] = t('Position') . ': ' . filter_xss_admin($settings['horizontal_position']) . ' - ' . filter_xss_admin($settings['vertical_position']);
  }
  $summary[] = t('!important declaration') . ': ' . (($settings['important']) ? t('Yes') : t('No'));

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 *
 * imagefield_css formatter changes the background of a region defined
 * by the selector, using the image loaded in the field.
 *
 * @see imagefield_css_formatter_info()
 */
function imagefield_css_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  foreach ($items as $delta => $item) {
    $variables = array(
      'item' => $item,
      'settings' => $display['settings'],
      'entity' => $entity,
    );
    theme_imagefield_css_formatter($variables);
  }
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - settings: field settings.
 *   - entity: Current entity needed to replace values with token module.
 *
 * @ingroup themeable
 */
function theme_imagefield_css_formatter($variables) {
  $item = $variables['item'];
  $settings = $variables['settings'];
  $entity = $variables['entity'];

  if ($settings['image_style']) {
    $url = image_style_url($settings['image_style'], $item['uri']);
  }
  else {
    $url = file_create_url($item['uri']);
  }

  $important = ($settings['important']) ? ' !important' : '';

  $inline_css = token_replace($settings['selector'], array('node' => $entity)) .
    '{background: ' . check_plain($settings['color']) . ' url("' .
    check_plain($url) . '") ' .
    check_plain($settings['repeat']) . ' ' .
    check_plain($settings['attachment']) . ' ' .
    check_plain($settings['horizontal_position']) . ' ' .
    check_plain($settings['vertical_position']) . $important . '}';
  drupal_add_css($inline_css, 'inline');
}
